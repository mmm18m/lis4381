# LIS4381

## Mary Meberg

### Project 2 Requirements:

1. Edit and delete items from database
2. Create edit_petstore.php and edit_petstore_process.php
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of validation

#### Home Screenshot:

|  *Home*: | 
|--------------------------------------------------|
| ![Website Home](img/promo.PNG)|

#### P2 Index Screenshots:

|  *Index*: | *Delete from Index*: | *Sucessful Delete from Index*: | 
|--------------------------------------------------|-------------------------------------|-------------------------------------|
| ![Website Home](img/index.PNG)| ![Invalid Data](img/delete.PNG)|![Invalid Data](img/delete_suc.PNG)|


#### Edit Petstore Screenshots:

|  *edit_petstore.php*: | *edit_petstore.php Failed Validation*:|  *Successful Edit*: | 
|--------------------------------------------------|-------------------------------------|-------------------------------------|
| ![Website Home](img/edit.PNG)| ![Invalid Data](img/data_invalid.PNG)|![Invalid Data](img/edited.PNG)|



#### RSS Screenshots

|  *RSS Feed*: | 
|-----------------|
|![Skillshot 10](img/rss.PNG)|


[Link to website](http://localhost/repos/lis4381/index.php)


