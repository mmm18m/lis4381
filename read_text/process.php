<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mary Meberg">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 5</title>
		<?php include_once("../css/include_css.php"); ?>
</head>


<?php 

$result = $_POST['comment'];

?>
<body>
<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

                    <p><?php print $result;?></p>

                    <?php include_once "global/footer.php"; ?>
</div> <!-- end starter-template -->
 </div> <!-- end container -->

</body>