# LIS4381

## Mary Meberg

### Assignment 5 Requirements:

1. Connect portfolio website to databse
2. Demonstrate client-side and server-side validation
3. Skillsets 13, 14, & 15
4. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of validation
* Provide Skillsets



#### Validation Screenshots:

|  *Screenshot of Pet Store Data List*: | *Screenshot of Server Validation*: | 
|--------------------------------------------------|-------------------------------------|
| ![Website Home](img/5_home.PNG)| ![Invalid Data](img/5_error.PNG)|


#### Skillset 13 Screenshots

|  *Skillset 13*: | 
|-----------------|
|![Skillshot 10](img/13.PNG)|

#### Skillset 14 Screenshots

|  *Addition (HOME)*: | *Addition (RESULT)*: |
|-----------------|----------------|
|![Skillshot 14 addition home](img/14_addition_home.PNG)|![Skillshot 14 addition results](img/14_addition_result.PNG)|

| *Division (HOME)*: | *Division (RESULT)*: |
|---------------|---------------|
|![Skillshot 14 division home](img/14_division_home.PNG)|![Skillshot 14 division result](img/14_division_result.PNG)|


#### Skillset 15 Screenshots

|  *Skillset 15 (HOME)*: | *Skillset 15 (RESULT)*: |
|-----------------|----------------|
|![Skillshot 15 home](img/15_write_home.PNG)|![Skillshot 15 results](img/15_write_result.PNG)|


[Link to website](http://localhost/repos/lis4381/index.php)

##### I didn't have time to create a nice design. Click the pig for a fun surprise instead!

[![Foo](https://media.istockphoto.com/photos/pig-picture-id120534386?k=6&m=120534386&s=612x612&w=0&h=nsy1ZpqPri6EfAaAOYgjLjpT5GjHGkgtEq0YTRiG6UU=)](https://www.youtube.com/watch?v=xgp3guuNsag)


