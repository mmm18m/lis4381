# LIS4381

## Mary Meberg

### Assignment 2 Requirements:

1. Create a mobile recipe app using Android Studio
2. Skillsets 1, 2, & 3
3. Chapter Questions (Chs 3,4)

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of mobile recipe app
* Provide Skillsets

#### Mobile App Screenshots:

|  *Screenshot of Mobile App 1*: | *Screenshot of Mobile App 2*: | 
|--------------------------------------------------|-------------------------------------|
| ![Mobile App 1](img/app1.PNG){ width=50%;}    | ![Mobile App 2](img/app2.PNG)|

#### Skillsets Screenshots
[Link to Skillsets](https://bitbucket.org/mmm18m/lis4381/src/master/a2/skillsets/)

|  *Skillset 1*: | *Skillset 2*: | *Skillset 3*: |
|-----------------|----------------|---------------|
|![Skillshot 1](img/oddeven.PNG)|![Skillshot 2](img/biggerint.PNG)|![Skillshot 3](img/loop.PNG)|
