import java.util.Scanner;

public class loop{

    public static void main(String[] args){
        //declare variables
        Scanner scnr = new Scanner(System.in);
        String [] exampleArray = {"Dog", "Cat", "Bird", "Fish", "Insect"};

        System.out.println("Developer: Mary Meberg");
            System.out.println("for loop:");
            for (int i = 0; i < 5; i++){
                System.out.println(exampleArray[i]);
            }
        System.out.println("");
        
        System.out.println("enhanced for loop");
                for (String i: exampleArray){
                    System.out.println(i);
                }
        System.out.println("");
        
        System.out.println("while loop");

            int count = 0;
            while(count < 5){
                System.out.println(exampleArray[count]);
                count++;
            }
        System.out.println("");

        System.out.println("do-while loop");

            int count2 = 0;
            do {
                System.out.println(exampleArray[count2]);
                count2++;
            } while (count2 < 5);
        System.out.println("");

        }
    }


