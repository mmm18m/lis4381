# LIS4381 - Mobile Web Application Development

## Mary Marguerite Meberg

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/a1/README.md)
    * Install AMPPS
* Install JDK
* Install Android Studio and create My First App
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/a2/README.md)
    * Provide Bitbucketread-only access to repos
    * Provide screenshots of mobile recipe app
    * Provide Skillsets 1, 2, & 3

3. [A3 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/a3/README.md)
    * Provide Screenshot of ERD
    * Provide Screenshots of Application
    * Provide Skillsets 4, 5, & 6

4. [A4 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/a4/README.md)
    * Provide Screenshots of Portfolio Website
    * Provide Skillsets 10, 11, & 12

5. [A5 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/a5/README.md)
    * Provide Screenshots of Portfolio Website (connected to database)
    * Provide Skillsets 13, 14, & 15


6. [P1 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/p1/README.md)
    * Create a "business card" app using Android Studio
    * Skillsets 7, 8, & 9

7. [P2 README.md](https://bitbucket.org/mmm18m/lis4381/src/master/p2/README.md)
    * Edit and delete items from database
    *	 Create a RSS Feed