# LIS4381

## Mary Meberg

### Project 1 Requirements:

1. Create a "business card" app using Android Studio
2. Skillsets 7, 8, & 9
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of "business card" app
* Provide Skillsets

#### Mobile App Screenshots:
|  *Screenshot of Mobile App 1*: | *Screenshot of Mobile App 2*: | 
|--------------------------------------------------|-------------------------------------|
| ![Mobile App 1](img/phone_1.PNG){ width=50%;}    | ![Mobile App 2](img/phone_2.PNG)|

#### Skillsets Screenshots
[Link to Skillsets](https://bitbucket.org/mmm18m/lis4381/src/master/p1/skillsets/)

|  *Skillset 7*: | *Skillset 8*: | *Skillset 9*: |
|-----------------|----------------|---------------|
|![Skillshot 7](img/skill_7.PNG)|![Skillshot 8](img/skill_8.PNG)|![Skillshot 9](img/skill_9.PNG)|