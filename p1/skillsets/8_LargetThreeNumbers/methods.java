import java.util.*;
public class methods {

    public static void printIntro(){
        System.out.println("Developer: Mary Meberg");
        System.out.println("Program evaluates largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.");
    }

    public static int[] genArray(){
        int[] userValues = new int[3];
        Scanner scnr = new Scanner(System.in);
        System.out.println("Please enter first int value:");
        do {
            try {
                userValues[0] = scnr.nextInt();
                while (userValues[0] <= 0){
                    System.out.println("Please insert a value above 0:");
                    userValues[0] = scnr.nextInt();
                }
            } catch (InputMismatchException e){
                System.out.println("Valid choices are whole numbers only");
                scnr.next();
            }
        } while (userValues[0] <= 0);

        System.out.println("Please enter second int value:");
        do {
            try {
                userValues[1] = scnr.nextInt();
                while (userValues[1] <= 0){
                    System.out.println("Please insert a value above 0:");
                    userValues[1] = scnr.nextInt();
                }
            } catch (InputMismatchException e){
                System.out.println("Valid choices are whole numbers only");
                scnr.next();
            }
        } while (userValues[1] <= 0);

        System.out.println("Please enter third int value:");
        do {
            try {
                userValues[2] = scnr.nextInt();
                while (userValues[0] <= 0){
                    System.out.println("Please insert a value above 0:");
                    userValues[2] = scnr.nextInt();
                }
            } catch (InputMismatchException e){
                System.out.println("Valid choices are whole numbers only");
                scnr.next();
            }
        } while (userValues[2] <= 0);


        return userValues;

    }

    public static void largestNum(){
        int[] userValues = new int[3];
        userValues = genArray();

        if (userValues[0] > userValues[1]){
            if (userValues[0] > userValues[2]){
                System.out.println(userValues[0] + " is bigger than " + userValues[1] + " and " + userValues[2] );
            } else if (userValues[0] < userValues[2]){
                System.out.println(userValues[2] + " is bigger than " + userValues[0] + " and " + userValues[1] );
            }

        }

        if (userValues[0] < userValues[1]){
            if (userValues[1] > userValues[2]){
                System.out.println(userValues[1] + " is bigger than " + userValues[0] + " and " + userValues[2] );
            } else if (userValues[1] < userValues[2]){
                System.out.println(userValues[2] + " is bigger than " + userValues[0] + " and " + userValues[1] );
            }

        }

    }

    }