import java.util.*;
public class methods {

    public static void printIntro() {
        System.out.println("Developer: Mary Meberg.");
        System.out.println("1) Program creates array size at run-time.");
        System.out.println("2) Program displays array size. ");
        System.out.println("3) Program rounds sum and average of numbers to two decimal places. ");
        System.out.println("4) Numbers *must* be float data type, not double. ");
        System.out.println("");
    }

    public static int getArraySize() {
        Scanner scnr = new Scanner(System.in);
        int arraySize = 0;
        System.out.println("Please enter array size:");
        do {
            try {
                arraySize = scnr.nextInt();
                while (arraySize <= 0){
                    System.out.println("Please insert a value above 0:");
                    arraySize = scnr.nextInt();
                }
            } catch (InputMismatchException e){
                System.out.println("Valid choices are whole numbers only");
                scnr.next();
            }
        } while (arraySize <= 0);

        return arraySize;

    }

    public static float[] userNumbers() {
        Scanner scnr = new Scanner(System.in);
        int arraySize = getArraySize();
        float[] userList = new float [arraySize];

        System.out.println("");
        System.out.println("Please enter " + arraySize + " numbers:");
        for (int i = 0; i < arraySize; i++){
            System.out.println("Please enter number " + (i+1));
                while (userList[i] == 0){
                    try {
                        userList[i] = scnr.nextFloat();
                        
                    } catch (InputMismatchException e){
                        System.out.println("Valid choices are numbers only. Please re-enter number " + (i+1));
                        scnr.next();
                    }
                }//end while

        }//end of for

        return userList;

    }//end of class

    public static void summaryList() {
        float[] userList = userNumbers();
        
        float sum = 0;

        System.out.println("");
        System.out.print("Numbers entered: ");
        for (int i = 0; i < userList.length; i++){
            System.out.print(userList[i] + " ");
        }
        System.out.println("");
        System.out.print("Sum of numbers: ");
        for (int i = 0; i < userList.length; i++){
            sum = sum + userList[i];
        }
      
        System.out.printf("%.2f", sum);
        System.out.println("");
        System.out.print("Average of numbers: ");
        System.out.printf("%.2f",(sum/userList.length));

    }//end of class
}