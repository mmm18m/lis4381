# LIS4381 - Mobile Web Application Development

## Mary Meberg

### **Assignment 1 Requirements:**

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### **README.md file should include the following items:**

* Screenshot of AMPSS Installation
* Screenshot of running java Hello
* Screemshot of running Andriod Studio -My First App
* git commands with short description
* Bitbucket repo links: a) this assignment and b)the completed tutorials above (bitbucketstationlocation)

#### Git commands w/short descriptions:

1. git init: Creates a new Git repository.
2. git status: Displays the state of the working directory and the staging area.
3. git add: Adds a change in the working directory to the staging area
4. git commit: Saves your changes to the local repository.
5. git push: Uploads local repository content to a remote repository.
6. git pull: Fetches and downloads content from a remote repository updates the local repository to match.
7. git branch: Lists all the branches in your repo, and tells you what branch you're currently in.

#### Assignment Screenshots:




|  *Screenshot of AMPPS running http://localhost*: | *Screenshot of running java Hello*: | *Screenshot of Android Studio - My First App*: |
|--------------------------------------------------|-------------------------------------|------------------------------------------------|
| ![AMPPS Installation Screenshot](img/php.PNG)    | ![JDK Installation Screenshot](img/helloworld.PNG) | ![Android Studio Installation Screenshot](img/myfirstapp.PNG) |

    


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mmm18m/stationlocation/src/master/ "Bitbucket Station Locations")
