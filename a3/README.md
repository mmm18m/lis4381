# LIS4381

## Mary Meberg

### Assignment 3 Requirements:


1. Create an app using Android Studio
2. Create an ERD in MySQL
2. Skillsets 4, 5, & 6
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of mobile recipe app
* Provide Skillsets

#### ERD Screenshots:
![ERDModel](img/model.PNG)

[pet.mwb](https://bitbucket.org/mmm18m/lis4381/src/master/a3/ERD_files/pet.mwb)

[pet.sql](https://bitbucket.org/mmm18m/lis4381/src/master/a3/ERD_files/pet.sql)

#### Mobile App Screenshots:
[Link to img resources in app](https://bitbucket.org/mmm18m/lis4381/src/master/a3/app_img_rsc/)

|  *Screenshot of Mobile App 1*: | *Screenshot of Mobile App 2*: | 
|--------------------------------------------------|-------------------------------------|
| ![Mobile App 1](img/phone_1.PNG){ width=50%;}    | ![Mobile App 2](img/phone_2.PNG)|

#### Skillsets Screenshots
[Link to Skillsets](https://bitbucket.org/mmm18m/lis4381/src/master/a3/skillsets/)

|  *Skillset 4*: | *Skillset 5*: | *Skillset 6*: |
|-----------------|----------------|---------------|
|![Skillshot 1](img/skill_4.PNG)|![Skillshot 2](img/skill_5.PNG)|![Skillshot 3](img/skill_6.PNG)|
