import java.util.Scanner;
public class Methods {

        public static void printIntro(){
            System.out.println("Program evaluates user-entered characters.");
            System.out.println("Use following characters: W or w, C or c, H or h, N or n.");
            System.out.println("Use following decision structures: If..else, and switch.");
            System.out.println("");
        }

        public static void phoneType(){
            Scanner scnr = new Scanner(System.in);
            System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
            System.out.println("Please enter phone type:");
            char selection = scnr.next().charAt(0);

        System.out.println("If/Else Loop:");
        if (selection == 'W' || selection == 'w'){
                System.out.println("Phone type: Work.");
            } else if (selection == 'C' || selection == 'c'){
                    System.out.println("Phone type: Cell.");
            } else if (selection == 'H' || selection == 'h'){
                    System.out.println("Phone type: Home.");
            } else if (selection == 'N' || selection == 'n'){
                    System.out.println("Phone type: None.");
            } else {
                    System.out.println("This is an invalid option.");
            }

        System.out.println("");
        System.out.println("Switch Statement:");
        switch (selection) {
            case 'w':
            case 'W':
                System.out.println("Phone type: Work.");
                break;
            case 'c':
            case 'C':
                System.out.println("Phone type: Cell.");
                break;
            case 'h':
            case 'H':
                System.out.println("Phone type: Home.");
                break;
            case 'n':
            case 'N':
                System.out.println("Phone type: None.");
                break;
            default:
                System.out.println("This is an invalid option");
        }
}
}


