import java.util.Scanner;
public class methodsJavaMethods {
    public static void getRequirements(){
        System.out.println("Program prompts user for first name and age, then prints results");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserInput(): Void method prompts for user input, then calls two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3) myvoidMethod():");
        System.out.println("    a. Accepts two arguments: String and int.");
        System.out.println("    b. Prints user's first name and age.");
        System.out.println("4) myValueReturningMethod():");
        System.out.println("    a. Accepts two arguments: String and int.");
        System.out.println("    b. Returns String containing first name and age.");
        System.out.println("");
    }

    public static void getUserInput(){
        Scanner scnr = new Scanner(System.in);
        String fname = "";
        int age = 0;
        System.out.println("Enter first name:");
        fname = scnr.next();
        System.out.println("Enter age:");
        age = scnr.nextInt();
        System.out.println("");
        myVoidMethod(fname, age);
        System.out.println("");
        System.out.println(myReturnMethod(fname, age));
    }

    public static void myVoidMethod(String fname, int age){
        System.out.println("void method call: " + fname + " is " + age);
    }

    public static String myReturnMethod(String fname, int age){
        String name;
        name = "value-returning method call: " + fname + " is " + age;
        return name;
    }
}
