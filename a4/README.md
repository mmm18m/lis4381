# LIS4381

## Mary Meberg

### Assignment 4 Requirements:

1. Create a portfolio website
2. Skillsets 10, 11, & 12
3. Chapter Questions

#### **README.md file should include the following items:**

* Provide Bitbucketread-only access to repos
* Provide screenshots of portfolio website
* Provide Skillsets

#### Mobile App Screenshots:

|  *Screenshot of Portfolio Website Home*: | *Screenshot of Valid Data Entry*: | *Screenshot of Invalid Data Entry*: | 
|--------------------------------------------------|-------------------------------------|-------------------------|
| ![Website Home](img/Capture.PNG)| ![Valid Data](img/valid.PNG)|![Invalid Data](img/invalid.PNG)|


#### Skillsets Screenshots

|  *Skillset 10*: | *Skillset 11*: | *Skillset 12*: |
|-----------------|----------------|---------------|
|![Skillshot 10](img/skillset10.PNG)|![Skillshot 11](img/skillset11.PNG)|![Skillshot 12](img/skillset12.PNG)|

[Link to website](http://localhost/repos/lis4381/index.php)
